job "${PREFIX}_centos8_snapshots" {
  datacenters = ["meyrin"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = false
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_centos8_snapshots" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/centos8_snapshots/centos8_snapshots:${CI_COMMIT_REF_NAME}"
      force_pull = ${FORCE_PULL}
      logging {
        config {
          tag = "${PREFIX}_centos8_snapshots"
        }
      }
      volumes = [
        "$DATA:/data",
        "$ADVISORIES:/advisories",
        "$CEPHTMP:/cephtmp",
        "/etc/nomad/submit_token:/secrets/token",
        "/etc/nomad/ssh-id_ed25519:/root/.ssh/id_ed25519",
        "/etc/nomad/linuxci_api_token:/secrets/linuxci_api_token"
      ]
    }

    env {
      PATH_MIRROR = "$PATH_MIRROR"
      PATH_SRC_RPMS = "$PATH_SRC_RPMS"
      PATH_DBG_RPMS = "$PATH_DBG_RPMS"
      PATH_SIGDBG_RPMS = "$PATH_SIGDBG_RPMS"
      PATH_KOJI_RPMS = "$PATH_KOJI_RPMS"
      PATH_DESTINATION = "$PATH_DESTINATION"
      PRODDATE = "$PRODDATE"
      OLDESTDATE = "$OLDESTDATE"
      EMAIL_FROM = "$EMAIL_FROM"
      EMAIL_ADMIN = "$EMAIL_ADMIN"
      EMAIL_USERS = "$EMAIL_USERS"
      EMAIL_USERS_TEST = "$EMAIL_USERS_TEST"
      NOMAD_ADDR = "$NOMAD_ADDR"
    }

    resources {
      cpu = 6000 # Mhz
      memory = 1024 # MB
    }

  }
}
