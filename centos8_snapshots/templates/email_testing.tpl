To: $EMAIL_USERS_TEST
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: [TEST][SECURITY]: C8X - $SHORTLIST [...]

Dear Linux users,

Today's CentOS Linux 8 (C8X) TEST system update contains the following packages:

 *******************************************************
 *** NOTE:                                           ***
 *** Packages listed below are provided for TESTS    ***
 *** ONLY, DO NOT USE THESE ON PRODUCTION SYSTEMS    ***
 *** these may NEVER be released to regular updates  ***
 *** and may be broken, requiring manual fixes on    ***
 *** on your system.                                 ***
 *******************************************************

--PACKAGES--

fixing multiple security vulnerabilities and/or providing bugfixes and enhancements.

For more information about vulnerabilities fixed please check:

 $WEBSITE/updates/c8/test/latest_updates

This TEST update can be applied on your system (providing it is set up to receive
test updates according to: $WEBSITE/updates/c8),
by running as root on your machine:

 # /usr/bin/dnf -y update

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
