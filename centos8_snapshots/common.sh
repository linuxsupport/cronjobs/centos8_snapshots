## Basic config file
TODAY=`/bin/date +%Y%m%d`
TODAY_FORMAT=`/bin/date +%Y-%m-%d`
DELAYED=`/bin/date +%Y%m%d --date="$PRODDATE"`
TOO_OLD=`/bin/date +%Y%m%d --date="$OLDESTDATE"`

SOURCE="/data/$PATH_MIRROR"
DESTINATION="/data/$PATH_DESTINATION"
SRC_RPMS="/data/$PATH_SRC_RPMS"
DBG_RPMS="/data/$PATH_DBG_RPMS"
SIGDBG_RPMS="/data/$PATH_SIGDBG_RPMS"
KOJI_RPMS="/data/$PATH_KOJI_RPMS"
SNAPS_DIR="8-snapshots"
SNAPS="$DESTINATION/$SNAPS_DIR"
DEST=$SNAPS/.tmp.$TODAY
TODELETE=$SNAPS/.todelete.$TODAY
FINALDEST=$SNAPS/$TODAY

CENTOS_REPOS="BaseOS AppStream PowerTools centosplus cr extras fasttrack HighAvailability Devel"
SIG_REPOS="cloud virt sclo messaging"
KOJI_REPOS="cern8-stable openafs8-stable"
CENTOS_ARCHES="x86_64 aarch64"
ALL_REPOS="CERN openafs $CENTOS_REPOS $SIG_REPOS"

LN_TESTING="testing"
WEBSITE="https://linux.cern.ch"

SHORTLIST_COUNT=4
MAX_PACKAGES_LIST=25
DANGEROUS_RPMS="centos-release centos-linux-release centos-linux-repos centos-gpg-keys epel-release elrepo-release \
  centos-release-\(openstack\|scl\|virt\|ovirt\|advanced-virtualization\|xen\|qemu\|messaging\).*"
HIGHLIGHT_RPMS="centos-linux-release kernel kernel-plus kernel-rt dbus glibc \
  cern-get-keytab hepix qemu-kvm systemd libvirt firewalld python sssd kmod-openafs"

addDays() {
  DAYS=${2:-1} # default to 1 day
  /bin/date +%Y%m%d --date="$1+$DAYS days"
}

diffRepos() {
  NEW=$1
  OLD=$2

  for f in `diff --new-line-format="" --unchanged-line-format="" \
      <(find $NEW -type f -iname "*.rpm" -printf "%P\n" | sort)  \
      <(find $OLD -type f -iname "*.rpm" -printf "%P\n" | sort)`; do
    rpm -qp --queryformat "$f;%{name};%{version};%{release};%{arch}\n" $NEW/$f 2>/dev/null
  done
}

snapshotKojiRepo() {
  REPO=$1
  for ARCH in $CENTOS_ARCHES; do
    NAME="${REPO%%8-stable}"
    if [[ $NAME == "cern" ]]; then
      NAME="CERN"
    fi

    # Create the new path
    mkdir -pv $DEST/$NAME/$ARCH

    # Copy the RPMs
    cp -Rl $KOJI_RPMS/$REPO/$ARCH/os/Packages $DEST/$NAME/$ARCH
    cp -Rl $KOJI_RPMS/$REPO/$ARCH/os/repodata $DEST/$NAME/$ARCH

    # Remove filtered packages
    kludgeRepository $NAME/$ARCH

    # Look for differences
    diffRepos $DEST/$NAME/$ARCH/Packages $SNAPS/$YESTERDAY/$NAME/$ARCH/Packages > $DEST/$NAME/$ARCH/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$NAME/$ARCH/.diff" ] || rm -f "$DEST/$NAME/$ARCH/.diff"

    # If we already have a Source directory, it's because we copied it for a previous $ARCH, so we can continue
    [[ -d $DEST/$NAME/Source ]] && continue

    # Copy sources (as 'Source' to match upstream repos)
    mkdir -pv $DEST/$NAME/Source
    cp -Rl $KOJI_RPMS/$REPO/source/SRPMS/* $DEST/$NAME/Source
    diffRepos $DEST/$NAME/Source $SNAPS/$YESTERDAY/$NAME/Source > $DEST/$NAME/Source/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$NAME/Source/.diff" ] || rm -f "$DEST/$NAME/Source/.diff"
  done
}

# Try to delete faster than just a straight `rm -rf`
quickDelete() {
  echo "Deleting $1"
  time for d in `find $1 -depth -type d 2>/dev/null`; do
    pushd $d >/dev/null
    perl -e 'for(<*>){unlink}'
    popd >/dev/null
    rm -rf $d
  done
}
