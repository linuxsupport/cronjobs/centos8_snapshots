#!/bin/bash

source /root/common.sh

getLinkDate() {
  readlink "$1" | sed "s/$SNAPS_DIR\///"
}

isLinkNotFrozen() {
  ! ([ -f "$DESTINATION/.freeze.8all" ] || [ -f "$DESTINATION/.freeze.$1" ])
}

setLink() {
  TARGET=$1
  SOURCE=$2
  TARGET_DATE=`echo $TARGET | sed "s/$SNAPS_DIR\///"`
  SOURCE_DATE=`getLinkDate $SOURCE`

  if isLinkNotFrozen $SOURCE && [[ $TARGET_DATE -gt $SOURCE_DATE ]]; then
    echo -n " Link "
    ln -vfsT $TARGET $SOURCE

    # Aggregate all the diffs between $SOURCE_DATE and $TARGET_DATE
    DATE=`addDays $SOURCE_DATE`
    while [[ $DATE -le $TARGET_DATE ]]; do
      for REPO in $ALL_REPOS; do
        for ARCH in $CENTOS_ARCHES; do
          if [[ -d "$SNAPS/$DATE/$REPO/$ARCH/os" ]]; then
            DIFF="$SNAPS/$DATE/$REPO/$ARCH/os/.diff"
          else
            DIFF="$SNAPS/$DATE/$REPO/$ARCH/.diff"
          fi
          cat "$TMPDIR/.diff:${SOURCE}:${REPO}:${ARCH}" $DIFF 2>/dev/null | sort -u -o "$TMPDIR/.diff:${SOURCE}:${REPO}:${ARCH}"
        done

        # If there's no diff, just delete the file
        [ -s "$TMPDIR/.diff:${SOURCE}:${REPO}:${ARCH}" ] || rm -f "$TMPDIR/.diff:${SOURCE}:${REPO}:${ARCH}"
      done
      DATE=`addDays $DATE`
    done

    return 0
  fi
  return 1
}

cd $DESTINATION

if [ -f "$DESTINATION/.freeze.8all" ]; then
  # A .freeze.8all file exists, so validation or triggers failed
  # Let's make sure today's snapshot exists, even if it's as a symlink to yesterday
  cd $SNAPS
  YESTERDAY=`/bin/date +%Y%m%d --date='yesterday'`
  ln -vfsT $YESTERDAY $TODAY

  echo ".freeze.8all exists, validation or trigger error. Not updating links."
  exit
fi

# No .freeze.8all file, so all went well
# Move $TODAY snapshot to a directory to be later deleted
mv -v $FINALDEST $TODELETE 2>/dev/null
# Promote .tmp.$TODAY to $TODAY
mv -v $DEST $FINALDEST
# Relink .s8-latest to today's snapshot
ln -vfsT $SNAPS_DIR/$TODAY ".8-latest"

# Don't update links on holidays
/root/checkholidays.sh || exit

echo "Checking symlinks"

if [[ -f "$DESTINATION/.forcemove.8" ]]; then
  # We're being forced to move the production symlinks, accelerating the
  # deployment to production. Let's do some sanity checks first.
  FORCED=`cat "$DESTINATION/.forcemove.8"`
  echo "Found .forcemove.8, should move production symlinks to $SNAPS_DIR/$FORCED"
  if [[ ! -d "$SNAPS_DIR/$FORCED" ]]; then
    echo "That path does not exist or is not a directory, won't do it!"
    exit 1
  fi
  echo "Ok, directory exists, production symlinks will point to $FORCED"
  # Note that symlinks are *never* moved backwards in time, no matter
  # what the .forcemove.8 file says. setLink() enforces this.
  DELAYED=$FORCED
  rm "$DESTINATION/.forcemove.8"
fi

TMPDIR=`mktemp -d`

RELEASE=`readlink $SOURCE | sed 's/\/$//'`       # ie. 8.3.2011
RELEASE_SHORT=`echo $RELEASE | cut -d'.' -f1,2`  # ie. 8.3
RELEASE_SHORTEST=`echo $RELEASE | cut -d'.' -f1` # ie. 8

# 8-testing is always today, 8 is always delayed
setLink $SNAPS_DIR/$TODAY "$RELEASE_SHORTEST-$LN_TESTING"
SHORTEST_TEST=$?
setLink $SNAPS_DIR/$DELAYED "$RELEASE_SHORTEST"
SHORTEST_PROD=$?

# The testing point release links point to today's snapshot
setLink $SNAPS_DIR/$TODAY "$RELEASE_SHORT-$LN_TESTING"
SHORT_TEST=$?
setLink $SNAPS_DIR/$TODAY "$RELEASE-$LN_TESTING"
LONG_TEST=$?

# If the $RELEASE_SHORT (ie. 8.3) symlink doesn't exist already, it's because today's
# snapshot is a new point release. So, we create the two point release symlinks pointing
# to today.
# If it does exist, then nothing has changed and today's symlink should still be $DELAYED
[[ -h "$RELEASE_SHORT" ]] && PiT="$DELAYED" || PiT="$TODAY"
setLink $SNAPS_DIR/$PiT "$RELEASE_SHORT"
SHORT_PROD=$?
setLink $SNAPS_DIR/$PiT "$RELEASE"
LONG_PROD=$?

# Find any old point releases
OLD_POINTS=`find . -maxdepth 1 -type l -name "$RELEASE_SHORTEST.*" -not -name "*-$LN_TESTING" -not -name "$RELEASE_SHORT*" | sed 's/\.\///'`
for OLD in $OLD_POINTS; do
  [ -h "$OLD-$LN_TESTING" ] || continue

  OLD_DATE=`getLinkDate $OLD`
  OLD_TESTING_DATE=`getLinkDate "$OLD-$LN_TESTING"`
  OLD_MOD_DATE=`stat -c '%y' $OLD | sed 's/\(-\| .*\)//g'`

  # If the old point release is still not in line with it's -testing equivalent, advance it
  # Unless you've already done so today, then leave it
  if [[ $OLD_TESTING_DATE -gt $OLD_DATE ]] && [[ $DELAYED -ge $OLD_TESTING_DATE ]] && [[ $TODAY -gt $OLD_MOD_DATE ]]; then
    setLink "$SNAPS_DIR/$OLD_TESTING_DATE" $OLD
    OLD_PROD=$?
  fi
done

if [[ $SHORTEST_TEST == 0 ]] || [[ $SHORT_TEST == 0 ]] || [[ $LONG_TEST == 0 ]]; then
  /root/regen-repos.sh "c${RELEASE_SHORTEST}-${LN_TESTING}-.*"
  /root/sendemail.sh $TMPDIR $LN_TESTING
fi

if [[ $SHORTEST_PROD == 0 ]] || [[ $SHORT_PROD == 0 ]] || [[ $LONG_PROD == 0 ]] || [[ $OLD_PROD == 0 ]]; then
  /root/regen-repos.sh "c${RELEASE_SHORTEST}-.*" ".*8-build"
  /root/sendemail.sh $TMPDIR
fi

# Clean up .diff:* files
find "$TMPDIR" -name '.diff:*' -delete
