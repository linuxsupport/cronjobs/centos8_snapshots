#!/bin/bash

source /root/common.sh

YESTERDAY=`/bin/date +%Y%m%d --date='yesterday'`

cd $SNAPS
COUNTER=0
while [[ ! -e "$YESTERDAY" && $COUNTER -le 30 ]]; do
    PREV=`addDays $YESTERDAY -1`
    echo "Snapshot for $YESTERDAY doesn't exist, linking to $PREV"
    ln -vfsT $PREV $YESTERDAY
    YESTERDAY="$PREV"
    COUNTER=$((COUNTER + 1))
done
