#!/bin/bash

source /root/common.sh

TMPDIR=$1
if [[ $2 == "daily" ]]; then
  CHANNEL="daily"
  TEMPLATE="/root/templates/email_dailydiff.tpl"
elif [[ $2 == $LN_TESTING ]]; then
  CHANNEL="testing"
  TEMPLATE="/root/templates/email_testing.tpl"
else
  CHANNEL="production"
  TEMPLATE="/root/templates/email_production.tpl"
fi


for ARCH in $CENTOS_ARCHES; do
  for REPO in $ALL_REPOS; do
    if [[ $CHANNEL == "daily" ]]; then
      SEARCH="-path '*/$REPO/$ARCH/*' -name .diff"
    elif [[ $CHANNEL == "testing" ]]; then
      SEARCH="-name '.diff:*-$LN_TESTING:$REPO:$ARCH'"
    else
      SEARCH="-name '.diff:*:$REPO:$ARCH' -not -name '.diff:*-$LN_TESTING:*'"
    fi
    eval find "$TMPDIR" $SEARCH -exec cat {} '\;' | sort -fuo "$TMPDIR/.diff:${REPO}:${ARCH}"

    # Check if .diff file is empty
    if [[ ! -s "$TMPDIR/.diff:${REPO}:${ARCH}" ]]; then
      rm -f "$TMPDIR/.diff:${REPO}:${ARCH}"
      continue
    fi

    cut -d';' -f2 "$TMPDIR/.diff:${REPO}:${ARCH}" >> "$TMPDIR/.longlist"

    # Log the diff for posterity
    awk -F';' -v REPO=$REPO -v ARCH=$ARCH '{print "Repodiff: "REPO" "ARCH": "$2" "$3" "$4" "$5}' "$TMPDIR/.diff:${REPO}:${ARCH}"
  done
done

STATE=""
while IFS= read -r LINE; do
  MODTIME="`echo $LINE | cut -d'.' -f1`"
  LINK="`echo $LINE | cut -d' ' -f2`"
  TARGET="`readlink $DESTINATION/$LINK`"
  STATE="$STATE  `printf '%-20s' "$LINK"` -> $TARGET (modified $MODTIME)\n"
done <<< "`find $DESTINATION -maxdepth 1 -type l -name '8*' -printf '%T+ %P\n' | sort | tail -n 10`"
# grab the last 10 symlinks so we can see the state of the previous point release as well

FROZEN=`find $DESTINATION -maxdepth 1 -type f -name '.freeze.8*' -print -quit`
if [[ -n "$FROZEN" ]]; then
  STATE="$STATE\n ***************************************************\n"
  STATE="$STATE ***  .freeze.8* exists, links won't be updated  ***\n"
  STATE="$STATE ***************************************************\n"
fi
export STATE="`printf "$STATE"`"

# There are no diffs to report on, we can stop
if [[ ! -s "$TMPDIR/.longlist" ]]; then
  if [[ $CHANNEL == "daily" ]]; then
    export NOPKG_SUBJECT_PREFIX="[DIFF]"
  elif [[ $CHANNEL == "testing" ]]; then
    export NOPKG_SUBJECT_PREFIX="[TEST]"
  else
    export NOPKG_SUBJECT_PREFIX="[SECURITY]"
  fi
  # But let's inform the admins (so they don't worry unneccessarily)
  envsubst < /root/templates/email_nopackages.tpl >> "$TMPDIR/.email"
  cat "$TMPDIR/.email" | ssmtp -t -v
  rm "$TMPDIR/.email"
  exit
fi

# Update website
if [[ $CHANNEL == "production" ]] || [[ $CHANNEL == "testing" ]]; then
  /root/commit-updates-to-docs.sh $TMPDIR $CHANNEL
fi

# Now lets assemble the list of highlights for the email's subject
cat "$TMPDIR/.longlist" | sort -fuo "$TMPDIR/.longlist"
SHORTLIST=`grep -f <(echo -n "$HIGHLIGHT_RPMS" | sed 's/\([^[:space:]]\+\) \?/^\1$\n/g') "$TMPDIR/.longlist" | head -n $SHORTLIST_COUNT | xargs`
LEN_SHORTLIST=`echo "$SHORTLIST" | awk -F" " '{printf "%d",NF}'`
if [[ $LEN_SHORTLIST -lt $SHORTLIST_COUNT ]]; then
  D=`expr $SHORTLIST_COUNT - $LEN_SHORTLIST`
  SHORTLIST="$SHORTLIST `grep -v -f <(echo -n "$HIGHLIGHT_RPMS" | sed 's/\([^[:space:]]\+\) \?/^\1$\n/g') "$TMPDIR/.longlist" | head -n $D | xargs`"
fi
rm "$TMPDIR/.longlist"

echo "Sending email to $CHANNEL users"

export TODAY="$TODAY"
export SHORTLIST="`echo "$SHORTLIST" | sed 's/\b\s\+\b/, /g'`"
export WEBSITE

envsubst < $TEMPLATE | sed '/^--PACKAGES--$/Q' > "$TMPDIR/.email"
for ARCH in $CENTOS_ARCHES; do
  for REPO in $ALL_REPOS; do
    [ -e "$TMPDIR/.diff:${REPO}:${ARCH}" ] || continue

    echo "$REPO $ARCH repository:" >> "$TMPDIR/.email"
    # LOS-483 Do not include i686 packages in the email
    head -n $MAX_PACKAGES_LIST "$TMPDIR/.diff:${REPO}:${ARCH}" | awk -F';' '$5~/i686/{ next; } {printf "  %-50s %s-%s\n", $2, $3, $4}' >> "$TMPDIR/.email"
    LINES=`wc -l "$TMPDIR/.diff:${REPO}:${ARCH}" | cut -d' ' -f1`
    if [[ $LINES -gt $MAX_PACKAGES_LIST ]]; then
      echo "   [... truncated list (${LINES} packages) ...]" >> "$TMPDIR/.email"
    fi
    echo >> "$TMPDIR/.email"

  done
done
envsubst < $TEMPLATE | sed '1,/^--PACKAGES--$/d' >> "$TMPDIR/.email"
cat "$TMPDIR/.email" | ssmtp -t -v
rm "$TMPDIR/.email"
