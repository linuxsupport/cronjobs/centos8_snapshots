#!/bin/bash

source /root/common.sh

echo "Checking for dangerous packages..."

TMPDIR=`mktemp -d`

find "$DEST" -not -path '*/CERN/*' -not -path '*/Source/*' -not -path '*/Debug/*' -name .diff -exec cat {} \; | sort -u  > $TMPDIR/alldiffs

while IFS= read -r LINE; do
  [[ -z "$LINE" ]] && continue
  while IFS= read -r RPM; do
    [ $RPM ] || continue
    FILE="`echo $RPM | cut -d';' -f 1`"
    N="`echo $RPM | cut -d';' -f 2`"
    A="`echo $RPM | cut -d';' -f 5`"
    echo "Dangerous package $FILE found in diff"

    [[ "$A" == "noarch" ]] && ARCHES="$CENTOS_ARCHES" || ARCHES="$A"

    for ARCH in $ARCHES; do
      # Assemble all the repos into a list of --repofrompath parameters for repoquery
      repos="`find "$DEST" -path "*/${ARCH}/*" -not -path '*/Source/*' -not -path '*/Debug/*' -not -path '*/.clean.*' -name 'repodata' | sed 's#/repodata##' | awk '{printf " --repofrompath=repo" NR "," $0}'`"

      latest_all=`repoquery --disablerepo='*' --enablerepo='repo*' $repos --show-duplicates --qf="%{name}-%{version}-%{release}" $N 2>/dev/null | sort -V | tail -n1`
      latest_cern=`repoquery --repoid=repo17 --repofrompath=repo17,${DEST}/CERN/${ARCH}/ --qf="%{name}-%{version}-%{release}" $N 2>/dev/null`

      if [[ "$latest_all" == "$latest_cern" ]]; then
        echo " -> Newest is from the CERN repo, can continue."
      else
        echo " -> No equivalent CERN package found!"
        DANGER="${DANGER}${FILE} "
      fi
    done

  done <<< "`grep ";$LINE;" $TMPDIR/alldiffs`"
done <<< "`echo "$DANGEROUS_RPMS" | tr ' ' '\n'`"

rm -rf $TMPDIR

if [ "$DANGER" != "" ]; then
  DANGER=`printf "$DANGER" | sort -u | cut -d ';' -f 1`
  echo "Found some dangerous RPMs, creating .freeze.8all"
  touch "$DESTINATION/.freeze.8all"

  export PACKAGES="`echo -n "$DANGER" | tr ' ' '\n'`"
  envsubst < /root/templates/email_tripwire.tpl | ssmtp -t
fi
