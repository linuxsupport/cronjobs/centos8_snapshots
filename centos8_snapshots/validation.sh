#!/bin/bash

source /root/common.sh

TMPROOT="/cephtmp/c8installroot"
DNF="dnf --installroot=$TMPROOT --releasever=8 --nogpgcheck --disablerepo=* --repofrompath=validation.cern,$DEST/CERN/x86_64 --repofrompath=validation.baseos,$DEST/BaseOS/x86_64/os --repofrompath=validation.appstream,$DEST/AppStream/x86_64/os"
TEMPLATE="/root/templates/email_validation.tpl"
TEMPLATE_WARNING="/root/templates/email_validation_warning.tpl"

run_test () {
  COMMAND=$1

  $DNF $COMMAND &> $TMPROOT/.output
  RET=$?
  grep -q '^Installed size:' $TMPROOT/.output; INSTALL=$?
  if [[ $RET -eq 0 ||  # 0 is ok
        ( $RET -eq 1 && $INSTALL -eq 0 )  # 1 may still be ok
      ]]; then
    RET=0
    rm -rf $TMPROOT/.output
  else
    # Log the errors
    cat $TMPROOT/.output
  fi

  return $RET
}

error () {
  echo "Encountered an error..."
  touch "$DESTINATION/.freeze.8all"

  export VALIDATION_EMAIL_TEXT=$1

  envsubst < $TEMPLATE | sed '/^--ERROR--$/Q' > "$TMPROOT/.email"
  tail -n 200 "$TMPROOT/.output" >> "$TMPROOT/.email"
  envsubst < $TEMPLATE | sed '1,/^--ERROR--$/d' >> "$TMPROOT/.email"

  cat "$TMPROOT/.email" | ssmtp -t
  exit
}

warning () {
  echo "Encountered an issue..."

  export VALIDATION_EMAIL_TEXT=$1

  envsubst < $TEMPLATE_WARNING | sed '/^--ERROR--$/Q' > "$TMPROOT/.email"
  tail -n 200 "$TMPROOT/.output" >> "$TMPROOT/.email"
  envsubst < $TEMPLATE_WARNING | sed '1,/^--ERROR--$/d' >> "$TMPROOT/.email"

  cat "$TMPROOT/.email" | ssmtp -t
  exit
}

[ -d $TMPROOT ] && quickDelete $TMPROOT
mkdir $TMPROOT

echo "Performing basic repository validation tests..."

# install dnf into a temporary installroot
if ! run_test "-y install dnf"; then
  error "bootstap dnf within the installroot"
fi

# dnf was installed in the installroot, let's now try to install cern groups
if ! run_test "--assumeno group install cern-base"; then
  error "install the group 'cern-base'"
fi

if ! run_test "--assumeno group install cern-addons"; then
  error "install the group 'cern-addons'"
fi

if ! run_test "--assumeno group install cern-developer-workstation"; then
  error "install the group 'cern-developer-workstation'"
fi

if ! run_test "--repofrompath=validation.openafs,$DEST/openafs/x86_64 --assumeno install kmod-openafs"; then
  error "install openafs"
fi

while IFS= read -r RPM; do
  [[ -z "$RPM" ]] && continue

  # Assemble all the repos into a list of --repofrompath parameters
  repos="`find "$DEST" -path "*/x86_64/*" -not -path '*/Source/*' -not -path '*/Debug/*' -not -path '*/.clean.*' -name 'repodata' | sed 's#/repodata##' | awk '{printf " --repofrompath=repo" NR "," $0}'`"

  if ! run_test "$repos --assumeno install ${RPM}"; then
    error "install ${RPM}"
  fi
done <<< "`sed 's/^\s*//;s/\s*$//;s/\s*#.*$//;/^\s*$/d' /root/packages_tested.lst`"

# Get almost all the group IDs
ALL_GROUPS=`$DNF group list -v --hidden | grep '^   ' | sed 's/.*(\(.*\))$/\1/' | grep -v '\(conflicts-\|cern-base\|cern-addons\|cern-developer-workstation\)'`

for GROUP in $ALL_GROUPS; do
  # Pretend to install the group, but all we really want is the solver debug data
  run_test "--assumeno group install --debugsolver $GROUP"

  # Check the solver results to see if there are problems
  grep -qw '^problem' debugdata/rpms/solver.result
  RES=$?

  # Clean up the debugdata
  rm -rf debugdata/

  # If 'problem' was not found in the results, grep returns 1 and we're happy
  if [[ $RES -ne 1 ]]; then
    warning "group install $GROUP"
  fi
done

echo "All tests passed."
quickDelete $TMPROOT
